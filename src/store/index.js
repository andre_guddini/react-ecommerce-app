import { configureStore } from "@reduxjs/toolkit";
import { createSlice } from "@reduxjs/toolkit";


const initialProductState = {quantity:0, showCart:false, items:[],notification:null, changed:false, selectedCategory: null};

const productSlice = createSlice({
    name:'product',
    initialState: initialProductState,
    reducers:{
        replaceCart(state,action){
            state.quantity = action.payload.quantity;
            state.items = action.payload.items;
        },
        addProduct(state,action){
            const newItem = action.payload;
            const existingItem = state.items.find(item => item.id === newItem.id);
            state.quantity++;
            state.changed = true;
            if(!existingItem){
                state.items.push({
                    id:newItem.id,
                    price:newItem.price,
                    quantity:1,
                    totalPrice: newItem.price,
                    name: newItem.title,
                    image: newItem.image
                });
            }else{
                existingItem.quantity++;
                existingItem.totalPrice = existingItem.price * existingItem.quantity;
            }
        },
        removeProduct(state,action){
            const id = action.payload;
            const existingItem = state.items.find(item => item.id === id);
            state.quantity--;
            state.changed = true;
            if(existingItem.quantity ===1){
                state.items = state.items.filter(item => item.id !== id);
            }else{
                existingItem.quantity--;
                existingItem.totalPrice = existingItem.totalPrice - existingItem.price;
            }
        },
        toggleCart(state){
            state.showCart = !state.showCart;
            state.changed = false;
        },
        closeCart(state){
            state.showCart = false;
            state.changed = false;
        },
        clearCart(state){
            state.changed = true;
            state.items = [];
            state.quantity = 0;
        },
        selectCategory(state, action){
            if(state.selectedCategory !== action.payload){
                state.selectedCategory = action.payload;
            }else{
                state.selectedCategory = null
            }
           
        }
    }
});
const loginSlice = createSlice({
    name:'login',
    initialState:{isLogged:false, showLoginForm:false,user:''},
    reducers:{
        logIn(state,action){
            state.isLogged = true;
            state.user = action.payload;
        },
        logOut(state){
            state.isLogged = false;
        },
        toggleLoginForm(state){
            state.showLoginForm = !state.showLoginForm;
        },
        closeLoginForm(state){
            state.showLoginForm = false;
        }
    }
})

const notificationSlice = createSlice({
    name:'notification',
    initialState:{},
    reducers:{
        showNotification(state,action){
            state.notification = {
                status: action.payload.status,
                title: action.payload.title,
                message: action.payload.message
            }
        }
    }
});
const languageSlice = createSlice({
    name:'languageSelect',
    initialState:{lang:'en'},
    reducers:{
        changeLanguage(state,action){
            state.lang = action.payload
        }
    }
})

const store = configureStore({
    reducer:{
     product: productSlice.reducer,
     notification: notificationSlice.reducer,
     login: loginSlice.reducer,
     language: languageSlice.reducer
    }
})

export const notificationActions = notificationSlice.actions;
export const productActions = productSlice.actions;
export const loginActions = loginSlice.actions;
export const languageActions = languageSlice.actions;
export default store;