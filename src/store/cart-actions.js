import { notificationActions } from ".";
import { productActions } from ".";

export const fetchCartData = () =>{         //завантаження початкового стану корзини з сервера
    return async(dispatch) =>{
        const fetchData = async () =>{
            const response = await fetch('https://ecommerce-8d80b-default-rtdb.europe-west1.firebasedatabase.app/cart.json');
            if(!response.ok){
                throw new Error('Could not fetch cart data');
            }
            const data = await response.json();

            return data;
        };
        try{
            const cartData = await fetchData();
            dispatch(productActions.replaceCart({
                items: cartData.items || [],   //бо якщо видалити всі дані з корзини, в бекенді видалиться значення items і ми будемо отримувати undefined
                quantity: cartData.quantity
            }));
        }catch(error){
            dispatch(
                notificationActions.showNotification({
                    status:'error',
                    title:'Cant get data from server',
                    message:'Fetching cart data failed'
                })
            );
        }
    }
}


export const sendCartData  = (cart) =>{
    return async (dispatch) => {
        dispatch(
            notificationActions.showNotification({
                status:'pending',
                title:'sending data',
                message:'Sending cart data'
            })
        );
        const sendRequest = async ()=> {
            const response = await fetch('https://ecommerce-8d80b-default-rtdb.europe-west1.firebasedatabase.app/cart.json' ,{
                method:'PUT',
                body:JSON.stringify({items:cart.items, quantity:cart.quantity})
            });
            if(!response.ok) {
                throw new Error('Sending cart data failed');
            }
        }
        try{
            await sendRequest();
            dispatch(
                notificationActions.showNotification({
                    status:'success',
                    title:'Success',
                    message:'Successfully sent cart data'
                })
            )
        }catch(error){
            dispatch(
                notificationActions.showNotification({
                    status:'error',
                    title:'Error!',
                    message:'Sending cart data failed'
                })
            )
        }
    }
}