import styles from './Footer.module.css'
import Button from '../UI/Button'
import instagramIcon from '../images/instagram-white.png'
import linkedInIcon from '../images/linkedin-white.png'
import facebookIcon from '../images/facebook-app-symbo-whitel.png'
import { useSelector } from 'react-redux'

const Footer = () =>{
    const selectedLanguage = useSelector((state) => state.language.lang); 
    const subscribeToNewsletterHandler = (event) =>{
         event.preventDefault();
    }
    return (
    <footer>
        <div className={styles.footerWrapper}>
                <div className={styles.footerFormContainer}>
                <h4>{selectedLanguage === 'en' ? 'Newsletter' :'Новини'}</h4>
                <p>{selectedLanguage === 'en' ? 'Subscribe to our newsletter' :'Підпишіться на наші новини'}</p>
                    <form action="#" className={styles.footerForm}>
                        <input type="text" placeholder={selectedLanguage === 'en' ? 'Your email' :'Ваш email'}></input>
                        <Button type="submit" onClick={subscribeToNewsletterHandler}>{selectedLanguage === 'en' ? 'Sign up' :'Підписатись'}</Button>
                    </form>
                </div>
                <div className={styles.footerFollows}>
                    <h4>{selectedLanguage === 'en' ? 'Follow us' :'Підпишіться на наші соцмережі'}</h4>
                    <div className={styles.footerIconsBlock}>
                        <a href="https://www.instagram.com/"><img src={instagramIcon} alt="footer-insta-icon"/></a>
                        <a href="https://www.linkedin.com/in/andrii-sasnyk-5b603a191/"><img src={linkedInIcon} alt="footer-linked-icon"/></a>
                        <a href="https://uk-ua.facebook.com/"><img src={facebookIcon} alt="footer-facebook-icon"/></a>
                    </div>
                </div>
            </div>
    </footer>
    )
}

export default Footer