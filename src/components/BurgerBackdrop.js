import styles from './BurgerBackdrop.module.css'
import ReactDom  from 'react-dom'


const portalElement = document.getElementById('overlays')
const BurgerBackdropComp = props =>{
    return <div className={styles.burgerBackdrop} onClick={props.onClose}></div>
}

const BurgerBackdrop = props =>{
    return <>
    {ReactDom.createPortal( <BurgerBackdropComp onClose={props.onClose}/>, portalElement )}
    </> 
}

export default BurgerBackdrop;