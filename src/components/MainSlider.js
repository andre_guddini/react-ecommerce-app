import Slider from 'react-slick';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import styles from './MainSlider.module.css'
import firstCarouselImg from "../images/carousel-4.jpg"
import secondCarouselImg from "../images/carousel-6.jpg"
import thirdCarouselImg from "../images/slide-3.jpg"
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';

const MainSlider = () => {
    const selectedLanguage = useSelector((state) => state.language.lang);
    const ShopText = selectedLanguage === 'en' ? 'Shop Now!' : 'Дивитись пропозиції'
    const settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                },
            },
        ],
    };
     

    return (
        <div className={styles.slider}>
            <Slider {...settings}>
                <div className={styles.slide} >
                   <Link to="/products"> <div className={styles.slideImg} style={{ backgroundImage: `url(${firstCarouselImg})` }}>
                    <p className={styles.slideDescr}>{ShopText}</p>
                    </div> </Link>
                    <h3>{selectedLanguage === "en" ? "Men's clothes 25% OFF!" : "Чоловічий одяг 25% знижки"}</h3>
                    <Link to="/products">{ShopText}</Link>
                </div>
                <div className={styles.slide}>
                <Link to="/products"> <div className={styles.slideImg} style={{ backgroundImage: `url(${secondCarouselImg})` }}>
                    <p className={styles.slideDescr}>{ShopText}</p>
                    </div> </Link>
                    <h3>{selectedLanguage === "en" ? "Women's clothes 30% OFF!" : "Жіночий одяг 30% знижки"}</h3>
                    <Link to="/products">{ShopText}</Link>
                </div>
                <div className={styles.slide}>
                <Link to="/products"> <div className={styles.slideImg} style={{ backgroundImage: `url(${thirdCarouselImg})` }}>
                    <p className={styles.slideDescr}>{ShopText}</p>
                    </div> </Link>
                    <h3>{selectedLanguage === "en" ? "Kid's clothes 20% OFF!" : "Дитячий одяг 20% знижки"}</h3>
                    <Link to ="/products">{ShopText}</Link>
                </div>
            </Slider>
        </div>
    );
};

export default MainSlider;