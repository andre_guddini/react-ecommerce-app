import { useDispatch } from "react-redux";
import { productActions } from "../store";
import styles from './CartItems.module.css';
import { useSelector } from "react-redux";

const CartItems = props =>{
    const dispatch = useDispatch();
    const selectedLanguage = useSelector((state) => state.language.lang); 
    const {title, quantity, total, price, id, image} = props.item;
    const addToCartHandler = () =>{
        dispatch(productActions.addProduct({
          id, //id :id це те саме, що просто id
          title,
          price,
        }));
      }
      const removeFromCartHandler = () =>{
        dispatch(productActions.removeProduct(id));
      }

    return (
<li className={styles['cart-item']}>
      <div>
        <h2>{title}</h2>
        <div className={styles.productContainer}>
        <div className={styles.cartImage}>
        <span className={styles.image}><img src={image} alt='cart-product'></img></span>
        </div>
        <div className={styles.summary}>
        
          <span className={styles.price}>{price} $</span>
          <span className={styles.amount}>x {quantity}</span>
          <span className={styles.amount}>{selectedLanguage === "en" ? "Total product price" :"Загальна вартість товарів"} {`${total.toFixed(2)} $`}</span>
        </div>
        <div className={styles.actions}>
        <button onClick={removeFromCartHandler}>−</button>
        <button onClick={addToCartHandler}>+</button>
      </div>
      </div>
      </div>
      
    </li>
    )
}

export default CartItems;