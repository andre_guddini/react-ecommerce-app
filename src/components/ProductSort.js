import menCategory from '../images/man-white.png';
import womenCategory from '../images/woman-clothes-white.png';
import electronicsCategory from '../images/circuit-white.png';
import jewelryCategory from '../images/diamond-ring-white.png';
import styles from './ProductSort.module.css';
import { useSelector,useDispatch } from 'react-redux';
import { productActions } from '../store';


const ProductSort = () => {
    const dispatch = useDispatch();
    const changeCategoryHandler = (category) =>{
       dispatch(productActions.selectCategory(category));
    }
    const selectedLanguage = useSelector((state) => state.language.lang);
    const selectedCategory = useSelector((state) => state.product.selectedCategory);
    return <>
        <div className={styles.sortingBlock}>
            <div className={`${styles.productCategory} ${selectedCategory === "men's clothing" ? styles.highlighted : ''}`}
             onClick={changeCategoryHandler.bind(null, "men's clothing")}>
                <p>{selectedLanguage === "en" ? "Men's clothing" :"Чоловічий одяг"}</p>
                <img src={menCategory} alt="men's-clothing" />
            </div>
            <div className={`${styles.productCategory} ${selectedCategory === "women's clothing" ? styles.highlighted : ''}`}
            onClick={changeCategoryHandler.bind(null, "women's clothing")}>
                <p>{selectedLanguage === "en" ? "Women's clothing" :"Жіночий одяг"}</p>
                <img src={womenCategory} alt="women's-clothing" />
            </div>
            <div className={`${styles.productCategory} ${selectedCategory === "jewelery" ? styles.highlighted : ''}`} 
            onClick={changeCategoryHandler.bind(null, "jewelery")}>
                <p>{selectedLanguage === "en" ? "Jewelry" :"Коштовності"}</p>
                <img src={jewelryCategory} alt="jewelry" />
            </div>
            <div className={`${styles.productCategory} ${selectedCategory === "electronics" ? styles.highlighted : ''}`} 
            onClick={changeCategoryHandler.bind(null, "electronics")}>
                <p>{selectedLanguage === "en" ? "Electronics" :"Електроніка"}</p>
                <img src={electronicsCategory} alt="electronics" />
            </div>
        </div>
    </>
}

export default ProductSort
