import classes from './Cart.module.css'
import Modal from './Modal';
import CartItems from './CartItems';
import CheckOut from './CheckOut';
import { useDispatch } from 'react-redux';
import { productActions } from '../store';
import { useSelector } from 'react-redux';
import { useState } from 'react';



const Cart = (props) => {
    const [checkoutReady, setCheckoutReady] = useState(false);
    const [isSubmitting, setIsSubmitting] = useState(false);
    const [didSubmit, setDidSubmit] = useState(false);
    const dispatch = useDispatch();

    const closeCartHandler = () => {
        dispatch(productActions.closeCart());
    }

    const clearCartHandler = () => {
        dispatch(productActions.clearCart());
    }

    const orderHandler = () => {
        setCheckoutReady(true);
    }
    const products = useSelector((state) => state.product.items);
    const selectedLanguage = useSelector((state) => state.language.lang); 
    let totalCartAmount = 0;
    const cartHasItems = products.length > 0;
    for (const item of products) {
        totalCartAmount += item.totalPrice;
    }

    const submitOrderHandler = async (userData) => {
        setIsSubmitting(true);
        await fetch('https://ecommerce-8d80b-default-rtdb.europe-west1.firebasedatabase.app/orders.json', {      //нема error handling тут, можна добавити
            method: 'POST',
            body: JSON.stringify({
                user: userData,
                orderderedItems: products
            })
        });
        setIsSubmitting(false);
        setDidSubmit(true);
        clearCartHandler()
    }

    const modalActions = <div className={classes.actions}>
        <button className={classes['button--alt']} onClick={closeCartHandler}>{selectedLanguage === "en" ? "Close" :"Закрити"}</button>
        {cartHasItems && <button className={classes.button} onClick={orderHandler}>{selectedLanguage === "en" ? "Order" :"Придбати"}</button>}
    </div>

    const cartItems =
        <ul>
            {products.map(item =>
                <CartItems key={item.id}
                    item={{ title: item.name, quantity: item.quantity, total: item.totalPrice, price: item.price, id: item.id, image:item.image }}
                />)}
        </ul>;

    const cartModalContent = <>
        {!cartHasItems && <p className={classes.total}>{selectedLanguage === "en" ? "Your cart is empty" :"Ваша корзина пуста"}</p>}
        {cartHasItems && <>
            {cartItems}
        <div className={classes.total}>
            <span>{selectedLanguage === "en" ? "Total amount" :"Загальна вартість"}</span>
            <span>{`${totalCartAmount.toFixed(2)} $`}</span>
        </div>
        {checkoutReady && < CheckOut onCancel={closeCartHandler} onConfirm={submitOrderHandler} />}
        {!checkoutReady && modalActions}
            </>}
        
    </>;

    const isSubmittingModalContent = <p>{selectedLanguage === "en" ? "Sending order data ..." :"Відправляємо ваше замовлення ..."}</p>;
    const didSubmitModalContent = <>
        <p>{selectedLanguage === "en" ? "Successfully sent the order!" :"Замовлення успішно відправлено!"}</p>
        <div className={classes.actions}>
            <button className={classes.button} onClick={closeCartHandler}>{selectedLanguage === "en" ? "Close" :"Закрити"}</button>
        </div>
    </>;

    return (
        <Modal onClose={closeCartHandler}>
            {!isSubmitting && !didSubmit && cartModalContent}
        {isSubmitting && isSubmittingModalContent}
        {didSubmit && didSubmitModalContent}
        </Modal>)
}




export default Cart;
