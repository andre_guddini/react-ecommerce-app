import { useDispatch } from "react-redux"
import { loginActions } from "../store"
import Modal from "./Modal"
import { useState } from "react"
import classes from './Login.module.css'
import LoginForm from "./LoginForm"
import { useSelector } from "react-redux"
import Button from "../UI/Button"
const Login = () => {
    const dispatch = useDispatch();
    const [isLogging, setIsLogging] = useState(false);
    const isLogged = useSelector(state => state.login.isLogged);
    const logName = useSelector(state => state.login.user.name);
    const selectedLanguage = useSelector((state) => state.language.lang);
    console.log(logName)
    const closeLoginFormHandler = () => {
        dispatch(loginActions.closeLoginForm());
    };

    const logUserOut = () => {
        dispatch(loginActions.logOut());
    }
    const submitLoginInfo = async (userData) => {
        setIsLogging(true);
        await fetch('https://ecommerce-8d80b-default-rtdb.europe-west1.firebasedatabase.app/users.json',
            {
                method: 'POST',
                body: JSON.stringify({
                    userInfo: userData
                })
            });
        setIsLogging(false);

    }

    const loggingInModalContent = <p>{selectedLanguage === 'en' ? 'Logging in ...' :'Авторизація ....'}</p>
    const loggedModalContent =
        <div className={classes.loginWelcome}>
            <p> {selectedLanguage === 'en' ? `You are logged as ${logName}` :`Ви авторизовані як ${logName}`}</p>
            <div className={classes.actions}>
                <Button type='button' onClick={logUserOut}>{selectedLanguage === 'en' ? 'Logout' :'Вийти'}</Button>
                <Button className={classes.button} onClick={closeLoginFormHandler}>{selectedLanguage === 'en' ? 'Close' :'Закрити'}</Button>
            </div>
        </div>;
    const loggedUserModal =
        <div className={classes.logInfo}>

        </div>
    const loginModalContent = <>
        {isLogged && loggedUserModal}
        {!isLogged && <LoginForm onClose={closeLoginFormHandler} onConfirm={submitLoginInfo} />}
    </>
    return (
        <Modal onClose={closeLoginFormHandler}>
            {!isLogging && !isLogged && loginModalContent}
            {isLogging && loggingInModalContent}
            {isLogged && loggedModalContent}
        </Modal>)
}

export default Login