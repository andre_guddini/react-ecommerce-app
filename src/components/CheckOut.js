import classes from './CheckOut.module.css';
import { useRef, useState } from 'react';
import { useSelector } from 'react-redux';
const isEmpty = value => value.trim() === '';
const isNotFiveChars = value => value.trim().length !== 5;
const phoneRegEx = /^\+(?:[0-9] ?){6,14}[0-9]$/;
const ukrainianPhoneRegEx =  /^(?:\+38)?0\d{9}$/;

const CheckOut = (props) => {
    const selectedLanguage = useSelector((state) => state.language.lang); 

    const [formInputsValidity, setFormInputsValidity] = useState({
        name: true,
        street: true,
        city: true,
        postalCode: true,
        phone:true
    })

    const nameInputRef = useRef();
    const streetInputRef = useRef();
    const postalInputRef = useRef();
    const cityInputRef = useRef();
    const phoneInputRef = useRef();
    const confirmHandler = (event) => {
        event.preventDefault();

        const enteredName = nameInputRef.current.value;
        const enteredStreet = streetInputRef.current.value;
        const enteredPostal = postalInputRef.current.value;
        const enteredCity = cityInputRef.current.value;
        const enteredPhone = phoneInputRef.current.value;

        const enteredNameIsValid = !isEmpty(enteredName);
        const enteredStreetIsValid = !isEmpty(enteredStreet);
        const enteredCityIsValid = !isEmpty(enteredCity);
        const enteredPostalIsValid = !isNotFiveChars(enteredPostal);
        const enteredPhoneIsValid = selectedLanguage === 'en'? phoneRegEx.test(enteredPhone) : ukrainianPhoneRegEx.test(enteredPhone);
        setFormInputsValidity({
            name: enteredNameIsValid,
            street: enteredStreetIsValid,
            city: enteredCityIsValid,
            postalCode: enteredPostalIsValid,
            phone: enteredPhoneIsValid
        })
        const formIsValid = enteredNameIsValid && enteredStreetIsValid & enteredCityIsValid && enteredPostalIsValid && enteredPhoneIsValid;

        if (!formIsValid) {
            return;
        }
        props.onConfirm({
            name: enteredName,
            street: enteredStreet,
            city: enteredCity,
            postalCode: enteredPostal,
            phone: enteredPhone
        });
    };
    const nameControlClasses = `${classes.control} ${formInputsValidity.name ? '' : classes.invalid}`;
    const streetControlClasses = `${classes.control} ${formInputsValidity.street ? '' : classes.invalid}`;
    const postalControlClasses = `${classes.control} ${formInputsValidity.postalCode ? '' : classes.invalid}`;
    const cityControlClasses = `${classes.control} ${formInputsValidity.city ? '' : classes.invalid}`;
    const phoneControlClasses = `${classes.control} ${formInputsValidity.phone ? '' : classes.invalid}`;

    return (
        <form className={classes.form} onSubmit={confirmHandler}>
            <h1>{selectedLanguage === "en" ? "Please confirm your order" :"Будь ласка підтвердіть ваше замовлення"}</h1>
            <div className={nameControlClasses}>
                <label htmlFor='name'>{selectedLanguage === "en" ? "Your name" :"Ваше ім'я"}</label>
                <input type='text' id='name' ref={nameInputRef} />
                {!formInputsValidity.name && <p>{selectedLanguage === "en" ? "Please enter a valid name" :"Будь ласка введіть дійсне ім'я"}</p>}
            </div>
            <div className={phoneControlClasses}>
                <label htmlFor='city'>{selectedLanguage === "en" ? "Phone number" :"Ваш номер телефону"}</label>
                <input type='tel' id='phone' ref={phoneInputRef} />
                {!formInputsValidity.phone && <p>{selectedLanguage === "en" ? "Please enter a valid phone number(starting with +)" :"Будь ласка введіть дійсний номер телефону(Формат +38**********)"}</p>}
            </div>
            <div className={cityControlClasses}>
                <label htmlFor='city'>{selectedLanguage === "en" ? "City" :"Ваше місто"}</label>
                <input type='text' id='city' ref={cityInputRef} />
                {!formInputsValidity.city && <p>{selectedLanguage === "en" ? "Please enter a valid city" :"Будь ласка введіть дійсну вулицю"}</p>}
            </div>
            <div className={streetControlClasses}>
                <label htmlFor='street'>{selectedLanguage === "en" ? "Street" :"Ваша вулиця"}</label>
                <input type='text' id='street' ref={streetInputRef} />
                {!formInputsValidity.street && <p>{selectedLanguage === "en" ? "Please enter a valid street" :"Будь ласка введіть дійсну вулицю"}</p>}
            </div>
            <div className={postalControlClasses}>
                <label htmlFor='postal'>{selectedLanguage === "en" ? "Postal code" :"Поштовий індекс"}</label>
                <input type='text' id='postal' ref={postalInputRef} />
                {!formInputsValidity.postalCode && <p>{selectedLanguage === "en" ? "Please enter a valid postal code(5 characters long)" :"Будь ласка введіть дійсний поштовий код(5 чисел)"} </p>}
            </div>
           
            <div className={classes.actions}>
                <button type='button' onClick={props.onCancel}>
                {selectedLanguage === "en" ? "Cancel" :"Скасувати"}
                </button>
                <button className={classes.submit}>{selectedLanguage === "en" ? "Confirm" :"Підтвердити"}</button>
            </div>
        </form>
    );
};

export default CheckOut;