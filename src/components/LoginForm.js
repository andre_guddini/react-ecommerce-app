import classes from './Login.module.css'
import Button from "../UI/Button"
import useInput from '../hooks/use-input';
import { useDispatch, useSelector } from 'react-redux';
import { loginActions } from '../store';


const isEmpty = value => value.trim() !== '';
const emailRegex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;


const LoginForm = (props) => {
    const dispatch = useDispatch();
    const selectedLanguage = useSelector((state) => state.language.lang);
    const logUserIn = () =>{
        dispatch(loginActions.logIn({
            name: enteredName,
            email: enteredEmail
        }));
    }

    const { value: enteredName,
        hasError: nameInputHasError,
        valueChangeHandler: nameChangedHandler,
        inputBlurHandler: nameBlurHandler,
        isValid: enteredNameIsValid,
        reset: resetNameInput
    } = useInput(isEmpty);

    const {
        value: enteredEmail,
        hasError: emailInputHasError,
        valueChangeHandler: emailChangedHandler,
        inputBlurHandler: emailInputBlurHandler,
        isValid: enteredEmailIsValid,
        reset: resetEmailInput
    } = useInput(value => emailRegex.test(value));
  
    let formIsValid = false;

    if (enteredNameIsValid && enteredEmailIsValid) {
        formIsValid = true;
    }
    const confirmLoginHandler = (event) => {
        event.preventDefault();
        if (!enteredNameIsValid || !enteredEmailIsValid) {
            return;
        }
        props.onConfirm({
            name: enteredName,
            email:enteredEmail
        })
        logUserIn();
        resetNameInput();
        resetEmailInput();
        
    }

  const nameInputClass = nameInputHasError ? `${classes.invalid}` : '';
  const emailInputClass = emailInputHasError ? `${classes.invalid}` : '';
    return (
        <div className={classes.logForm}>
            <h1>{selectedLanguage === 'en' ? 'Log in here' :'Авторизація'}</h1>
            <form onSubmit={confirmLoginHandler}>
                <input type="text" placeholder={selectedLanguage === 'en' ? 'Your name' :"Ваше ім'я "} className={nameInputClass} value={enteredName} onChange={nameChangedHandler} onBlur={nameBlurHandler} />
                {nameInputHasError && <p className={classes.errorText}>{selectedLanguage === "en" ? "Name must not be empty" :"Поле не повинно бути пустим"}</p>}
                <input type="email" placeholder={selectedLanguage === "en" ? "Your email" :"Ваш email "}className={emailInputClass} onChange={emailChangedHandler} value={enteredEmail} onBlur={emailInputBlurHandler} />
                {emailInputHasError && <p className={classes.errorText}>{selectedLanguage === "en" ? "Please enter a valid email" :"Будь ласка введіть дійсний email"}</p>}
                <Button type="submit" disabled={!formIsValid} className={classes.loginFormButton}>{selectedLanguage === "en" ? "Submit" :"Відправити"}</Button>
                <Button type="button" onClick={props.onClose} className={classes.loginFormButton}>{selectedLanguage === "en" ? "Cancel" :"Відмінити"}</Button>
            </form>
        </div>
    )

}



export default LoginForm;