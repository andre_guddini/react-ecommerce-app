import styles from './HeaderNavigation.module.css'
import { NavLink, Link } from 'react-router-dom';
import loginIcon from '../images/icon-profile-avatar-white.png';
import logOutIcon from '../images/login-icon-white.png'
import cartIcon from '../images/shopping-bag-white.png'
import BurgerBackdrop from './BurgerBackdrop';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { productActions, loginActions, languageActions } from '../store';

const HeaderNavigation = () => {
    const dispatch = useDispatch();
    const [isOpen, openMenu] = useState(false);
    const toggleCartHandler = () => {
        dispatch(productActions.toggleCart());
        }
    const toggleLanguageHandler = (event) => {
        dispatch(languageActions.changeLanguage(event.target.value));
    }
    const toggleNavMenu = () => {
        openMenu(!isOpen);
    }
    const closeMenu = () => {
        openMenu(false);
    };
    const toggleLoginForm = () =>{
        dispatch(loginActions.toggleLoginForm());
    }
    const totalCartItems = useSelector(state => state.product.quantity);
    const isLogged = useSelector((state) => state.login.isLogged);
    const selectedLanguage = useSelector((state) => state.language.lang);
    return <header>
        <nav className={styles.navbar}>
            <Link to='/' className={styles['nav-branding']}>Cool Shop</Link>
            <ul className={`${styles['nav-menu']} ${isOpen ? styles.open : ''}`}>
                <li className={styles['nav-item']}>
                    <NavLink to="/" className={({ isActive }) => isActive ? styles.active : undefined} end onClick={closeMenu}>{selectedLanguage === 'en' ? 'Home' :'Головна'}</NavLink>
                </li>
                <li className={styles['nav-item']}>
                    <NavLink to="/products" className={({ isActive }) => isActive ? styles.active : undefined} onClick={closeMenu}>{selectedLanguage === 'en' ? 'Products' :'Товари'}</NavLink>
                </li>
                <li className={styles['nav-item']}>
                    <NavLink to="reviews" className={({ isActive }) => isActive ? styles.active : undefined} onClick={closeMenu}>{selectedLanguage === 'en' ? 'Reviews' :'Відгуки'}</NavLink>
                </li>
                <li className={styles['nav-item']}>
                    <NavLink to="contacts" className={({ isActive }) => isActive ? styles.active : undefined} onClick={closeMenu}>{selectedLanguage === 'en' ? 'Contact' :'Побажання'}</NavLink>
                </li>
                <li className={styles['nav-item']}>
                    <button type="button"><img src={isLogged ? logOutIcon : loginIcon} alt="login-icon" className={styles.icon} onClick={toggleLoginForm}></img></button>
                </li>
                <li className={styles['nav-item']}>
                    <div className={styles.cartBtn}><button type="button" onClick={toggleCartHandler} ><img src={cartIcon} alt="cart-icon" className={styles.cart}></img></button>
                        <span className={styles.badge}>
                            {totalCartItems}
                        </span>
                    </div>

                </li>
                <li className={styles['nav-item']}>
                    <div className={styles.cartBtn}>
                    <select id="language" value={selectedLanguage} onChange={toggleLanguageHandler}>
                    <option value="en" className={styles.enIcon}>ENG     
                          </option>
                          <option value="ua" className={styles.uaIcon}>УКР      
                          </option>
                        </select>
                    </div>
                </li>

            </ul>
            <div className={styles.hamburger} onClick={toggleNavMenu}>
                <span className={styles.bar}></span>
                <span className={styles.bar}></span>
                <span className={styles.bar}></span>
            </div>
            {isOpen && <BurgerBackdrop onClose ={closeMenu} />}
        </nav>
    </header>
}

export default HeaderNavigation;