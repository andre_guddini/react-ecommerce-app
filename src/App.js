import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import RootLayout from './pages/Root';
import HomePage from './pages/HomePage';
import Products, { loader as productsLoader } from './pages/Products';
import SomeProductPageLoader, { loader as productLoader } from './pages/SomeProductLoader';
import Reviews from './pages/Reviews';
import ContactPage from './pages/ContactPage';
import { useSelector, useDispatch } from 'react-redux';
import { useEffect } from 'react';
import { sendCartData, fetchCartData } from './store/cart-actions';

const router = createBrowserRouter([
  {
    path: '/', element: <RootLayout />, children: [
      { index: true, element: <HomePage /> },
      { path: 'products', element: <Products />, loader: productsLoader },
      { path: 'products/:productId', element: <SomeProductPageLoader />, loader: productLoader },
      { path: 'reviews', element: <Reviews /> },
      { path: 'contacts', element: <ContactPage /> }
    ]
  }
])
let isInitial = true;

function App() {
  const dispatch = useDispatch();
  const cart = useSelector(state => state.product);

  useEffect(() => {
    dispatch(fetchCartData());
  }, [dispatch])

  useEffect(() => {
    if (isInitial) {
      isInitial = false;
      return //щоб зразу не оновлювалась корзина, при завантаженні компоненту
    }
    if (cart.changed) {
      dispatch(sendCartData(cart));
    }
  }, [cart, dispatch])

  return (
    <RouterProvider router={router} />
  );
}

export default App;
