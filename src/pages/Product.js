import styles from './Product.module.css';
import { Link } from 'react-router-dom';
import Button from '../UI/Button';
import { useDispatch, useSelector } from 'react-redux';
import { productActions } from '../store';
import Modal from '../components/Modal';
import { useState } from 'react';

const Product = (props) => {
    const dispatch = useDispatch();
    const [isModalOpen, setIsModalOpen] = useState(false);
    const addToCartHandler = (id, title, price, image) => { //переробити як action функцію, щоб без аргументів була
        dispatch(productActions.addProduct({
            id,
            title,
            price,
            image
        }))
        setIsModalOpen(true);
    }
    const closeCartHandler = () =>{
        setIsModalOpen(false);
    }
    const selectedLanguage = useSelector((state) => state.language.lang);
    const selectedCategory = useSelector((state) =>state.product.selectedCategory);
    const modalContentText = <div className={styles.productModal}>
 <h1>{selectedLanguage ==='en' ? 'Successfully added the product to the cart' : 'Товар успішно додано до корзини'}</h1> 
 <Button onClick={closeCartHandler}>Ok</Button>
    </div>;
    const products = selectedCategory ? props.products.filter(product => product.category === selectedCategory) : props.products
    return (<>
        {products.map((product) => (
            <div className={styles.productItem} key={product.id}>
                <Link to={`${product.id}`}>
                    <div className={styles.productImage} style={{ backgroundImage: `url(${product.image})` }}>
                        <p className={styles.productImageDescription}>{selectedLanguage === 'en' ? 'Product details' : 'Деталі товару'}</p>
                    </div>
                </Link>
                <div className={styles.productBody}>
                    <div className={styles.productTitle}><p><strong>{product.title}</strong></p></div>
                    <div className={styles.price}><p><strong>{selectedLanguage === 'en' ? 'Price' : 'Ціна'}</strong> {product.price}$</p></div>
                    <div className={styles.category}><p><strong>{selectedLanguage === 'en' ? 'Category' : 'Категорія'}</strong> {product.category}</p></div>
                    <Button type="button" className={styles.AddToCartBtn} id={product.id} onClick={addToCartHandler.bind(null, product.id, product.title, product.price, product.image)}>{selectedLanguage === 'en' ? 'Add to cart' : 'Додати в корзину'} </Button>
                    {isModalOpen && <Modal onClose={closeCartHandler} isTransparent={true} classes={`productmodal`}>
                       {modalContentText}   
                    </Modal>}
                </div>
            </div>
        ))}
    </>
    )
}

export default Product;


