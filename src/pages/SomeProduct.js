import styles from './SomeProduct.module.css';
import Button from '../UI/Button';
import { useDispatch, useSelector } from 'react-redux';
import { productActions } from '../store';
import { useState } from 'react';
import Modal from '../components/Modal';

const SomeProduct = (props) =>{
    const dispatch = useDispatch();
    const {product} = props;
    const selectedLanguage = useSelector((state) => state.language.lang);
    const [isModalOpen, setIsModalOpen] = useState(false); 
    const addToCartHandler = () =>{
        dispatch(productActions.addProduct({
          id:product.id, //id :id це те саме, що просто id
          title:product.title,
          price:product.price,
          image:product.image
        }));
        setIsModalOpen(true);
      }
    const closeCartHandler = () =>{
      setIsModalOpen(false);
    }
    const modalContentText = <div className={styles.productModal}>
    <h1>{selectedLanguage ==='en' ? 'Successfully added the product to the cart' : 'Товар успішно додано до корзини'}</h1> 
    <Button onClick={closeCartHandler}>Ok</Button>
       </div>;
      
    return ( <>
            <div className={styles.productItem} key={product.id}>
                <div className={styles.productImage}>
                    <img src={product.image} alt="product" />
                  </div>
            <div className={styles.productBody}>
                <div className={styles.productTitle}><p><strong>{product.title}</strong></p></div>
                <div className={styles.price}><p><strong>{selectedLanguage === 'en' ? 'Price:' :'Ціна:'}</strong> {product.price}</p></div>
                <div className={styles.category}><p><strong>{selectedLanguage === 'en' ? 'Category:' :'Категорія:'}</strong> {product.category}</p></div>
                <Button type="button" className={styles.AddToCartBtn} id={product.id} onClick={addToCartHandler}>{selectedLanguage === 'en' ? 'Add to cart' :'Додати в корзину'} </Button>
                {isModalOpen && <Modal onClose={closeCartHandler} isTransparent={true} classes={`productmodal`}>
                       {modalContentText}   
                    </Modal>}
            </div>
        </div> 
       </>
    )
}

export default SomeProduct

