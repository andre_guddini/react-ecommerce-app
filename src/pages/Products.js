import Product from "./Product";
import styles from './Product.module.css'
import { json,defer,useLoaderData,Await} from "react-router-dom";
import { Suspense } from "react";
import { useSelector } from "react-redux";
import ProductSort from "../components/ProductSort";

const Products = ()=>{
    const selectedLanguage = useSelector((state) => state.language.lang);
    const {products} = useLoaderData();
    return (<>
    <h1 className={styles.productHeading}>{selectedLanguage === "en" ? "Products" :"Наші Товари"}</h1>
    <ProductSort/>
    <div className={styles.products}>
    <Suspense fallback={<p style={{textAlign:'center'}}>{selectedLanguage === "en" ? "Loading..." :"Завантаження ..."}</p>}>
        <Await resolve={products}>{(loadedProducts) => <Product products={loadedProducts}/>}</Await>
        </Suspense>
        </div> 
    </>
    )
}

export default Products;


async function loadProducts(){
    const response = await fetch('https://fakestoreapi.com/products');
    if(!response.ok){
        throw json({message:`Can't load products`}, {status:500});
    }else{
        const responseData = await response.json();
        return responseData;
    }
}

export async function loader(){
    return defer({
        products: loadProducts()
    })
}





