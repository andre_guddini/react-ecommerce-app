import Slider from 'react-slick';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import styles from './Reviews.module.css'
import firstReviewImage from '../images/review1.jpg'
import secondReviewImage from '../images/review2.jpg'
import { useSelector } from 'react-redux';
const Reviews = () => {
    const selectedLanguage = useSelector((state) => state.language.lang); 

    const settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
       
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    arrows:false,
                },
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    arrows:false,
                },
            },
        ],
    };
     const reviewText = selectedLanguage === 'en' ? 'Client review' :'Відгук клієнта';
     
    return (<>
        <div className={styles.slider}>
            <Slider {...settings}>
                <div>
                    <div className={styles.slide} >
                        <div className={styles.slideImg}>
                            <img src={firstReviewImage} alt='client-review' />
                        </div>
                        <div className={styles.reviewBody}>
                            <h3 className={styles.reviewHeading}>{reviewText}</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quo, aut unde fugit, adipisci cupiditate at ducimus mollitia quae molestiae dolores suscipit labore eos perferendis corrupti! Dolorem eligendi quisquam nam eos eius quidem numquam voluptas, ducimus neque, consequuntur, earum explicabo? Nihil ullam odio molestias delectus tempore neque dolor!
                            </p>
                            <h4>John Marshall</h4>
                        </div>
                    </div>
                </div>
                <div>
                    <div className={styles.slide} >
                        <div className={styles.slideImg}>
                            <img src={secondReviewImage} alt='client-review' />
                        </div>
                        <div className={styles.reviewBody}>
                            <h3 className={styles.reviewHeading}>{reviewText}</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quo, aut unde fugit, adipisci cupiditate at ducimus mollitia quae molestiae dolores suscipit labore eos perferendis corrupti! Dolorem eligendi quisquam nam eos eius quidem numquam voluptas, ducimus neque, consequuntur, earum explicabo? Nihil ullam odio molestias delectus tempore neque dolor!
                            </p>
                            <h4>Nina Perry</h4>
                        </div>
                    </div>
                </div>
            </Slider>
        </div>
        </>
    );
};

export default Reviews;