import styles from './HomePage.module.css' ;
import MainSlider from '../components/MainSlider';
import { useSelector } from 'react-redux';
const HomePage = ()=>{
    const selectedLanguage = useSelector((state) => state.language.lang);
    return <>
    <h1 className={styles.mainPageHeading}>{selectedLanguage === 'en' ? 'Best deals right now!' :'Найкращі пропозиції зараз!'}</h1>
    <MainSlider/>
    </> 
}

export default HomePage;