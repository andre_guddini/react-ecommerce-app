import { Outlet } from "react-router-dom";
import HeaderNavigation from "../components/HeaderNavigation";
import Footer from "../components/Footer";
import Cart from "../components/Cart";
import Login from "../components/Login";
import { useSelector } from "react-redux";

const RootLayout = () =>{
    const isCartShown = useSelector((state) => state.product.showCart);
    const isLoginFormShown = useSelector((state) => state.login.showLoginForm);
    return <>
    {isCartShown && <Cart/>}
    {isLoginFormShown && <Login/>}
    <HeaderNavigation/>
    <main>
        <Outlet/>
    </main>
    <Footer/>
    </>
}

export default RootLayout;