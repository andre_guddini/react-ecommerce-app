import {useLoaderData, Await, json, defer } from "react-router-dom";
import { Suspense } from "react";
import SomeProduct from "./SomeProduct";
const SomeProductPageLoader = () => {
  const {product} = useLoaderData()
 return (
 <>
 <Suspense fallback={<p style={{textAlign:'center'}}>Loading</p>}>
 <Await resolve={product}>
    {loadedProduct => <SomeProduct product={loadedProduct} />}
 </Await>
 </Suspense>
 </>
 )
}

async function loadProduct(id){
    const response = await fetch('https://fakestoreapi.com/products/' + id);
    if(!response.ok){
     throw json({message: 'Could not fetch a data for this event'},{status:500});
    }
    else {
        const resData = await response.json();
        return resData
    }
}

export async function loader({request,params}){
    const id = params.productId;              
    console.log(id);
   return defer ({
     product: await loadProduct(id),       
   })
 }

export default SomeProductPageLoader;